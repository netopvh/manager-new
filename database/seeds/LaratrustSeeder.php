<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Limpando Tabela de Usuarios, Grupos, Perfis e Permissões');
        //$this->truncateLaratrustTables();

        $this->command->info('Criando Grupos');

        $grupos = [
            ['name' => 'Administração','parent_id' => null],
            ['name' => 'Usuários','parent_id' => 1],
            ['name' => 'Perfis','parent_id' => 1],
            ['name' => 'Permissões','parent_id' => 1],
            ['name' => 'Auditoria','parent_id' => 1],
        ];

        foreach ($grupos as $grupo) {
            \App\Domains\Access\Models\PermissionGroup::create([
                'name' => $grupo['name'],
                'parent_id' => $grupo['parent_id']
            ]);
        }

        $permissoes = [
            ['name' => 'ver-administracao', 'display_name' => 'Ver Administração', 'group_id' => 1],
            ['name' => 'ver-usuario', 'display_name' => 'Ver Usuários', 'group_id' => 2],
            ['name' => 'criar-usuario', 'display_name' => 'Criar Usuários', 'group_id' => 2],
            ['name' => 'editar-usuario', 'display_name' => 'Editar Usuários', 'group_id' => 2],
            ['name' => 'desativar-usuario', 'display_name' => 'Desativar Usuários', 'group_id' => 2],
            ['name' => 'ver-perfil', 'display_name' => 'Ver Perfil', 'group_id' => 3],
            ['name' => 'criar-perfil', 'display_name' => 'Criar Perfil', 'group_id' => 3],
            ['name' => 'editar-perfil', 'display_name' => 'Editar Perfil', 'group_id' => 3],
            ['name' => 'ver-permissoes', 'display_name' => 'Ver Permissões', 'group_id' => 4],
            ['name' => 'criar-permissoes', 'display_name' => 'Criar Permissões', 'group_id' => 4],
            ['name' => 'editar-permissoes', 'display_name' => 'Editar Permissões', 'group_id' => 4],
            ['name' => 'ver-auditoria', 'display_name' => 'Ver Auditoria', 'group_id' => 5]
        ];

        $permissions = [];

        $this->command->info('Criando Permissões');
        foreach ($permissoes as $permissao) {
            $permissions[] = \App\Domains\Access\Models\Permission::firstOrCreate([
                'name' => $permissao['name'],
                'display_name' => $permissao['display_name'],
                'group_id' => $permissao['group_id']
            ])->id;
        }

        $this->command->info('Criando Perfil Super Administrador');
        $roleSuper = \App\Domains\Access\Models\Role::create([
            'name' => 'Super Administrador',
            'display_name' => 'Super Administrador',
            'description' => 'Administrador Máximo do Sistema'
        ]);

        $this->command->info('Criando Perfil Gerente');
        $roleGerente = \App\Domains\Access\Models\Role::create([
            'name' => 'Gerente',
            'display_name' => 'Gerente',
            'description' => 'Gerente do Sistema'
        ]);

        $this->command->info('Usuário');
        $roleUser = \App\Domains\Access\Models\Role::create([
            'name' => 'Usuário',
            'display_name' => 'Usuário Comum',
            'description' => 'Usuário padrão do Sistema'
        ]);

        $this->command->info('Atribuindo Permissões');
        $roleSuper->permissions()->sync($permissions);
        $roleGerente->permissions()->sync($permissions);
        $roleUser->permissions()->sync($permissions);

        $this->command->info('Criando Usuários');
        $user = \App\Domains\Access\Models\User::create([
            'nome' => 'Angelo Neto',
            'username' => '1111',
            'email' => 'netopvh@gmail.com',
            'password' => bcrypt('123456'),
            'remember_token' => str_random(10),
            'active' => 1
        ]);

        $this->command->info('Atribuindo Perfil ao Usuário');
        $user->attachRole($roleSuper);
    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\Domains\Access\Models\User::truncate();
        \App\Domains\Access\Models\Role::truncate();
        \App\Domains\Access\Models\Permission::truncate();
        \App\Domains\Access\Models\PermissionGroup::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
