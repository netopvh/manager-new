@permission('editar-permissoes')
<a href="{{ route('permissions.edit',['id' => $permission->id]) }}" class="btn btn-xs btn-primary">
    <i class="icon-pencil7"></i>
</a>
@endpermission